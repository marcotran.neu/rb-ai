import pandas as pd
import streamlit as st
import matplotlib.pyplot as plt
from utils.youtube_utils import *
from utils.video_utils import *
from utils.video_id import *
from utils.sentiment import *

video_url = st.text_input('Youtube video url')
if video_url:
    thumbnail_url = get_thumbnail(video_url)
    st.image(
        thumbnail_url,
        width=400,  # Manually Adjust the width of the image as per requirement
    )
    video_id = get_yt_video_id(video_url)
    response_info_video = video_details(youtube,video_id)
    video_statistics = [video_statistic(response_info_video)]
    video_statistic_data = pd.DataFrame(video_statistics)
    st.text('SUMMARY')
    st.dataframe(video_statistic_data)
    st.text('VIDEO TRANSCRIPT: ' + str(get_transcript(video_id)))
    st.text('TYPE OF VIDEO: ' + str(get_type_of_video(video_id)))
    df_comments = scrape_comments_with_replies(video_id)
    data, positive, negative, neutral = sentiments(df_comments)
    st.text('Comment Analysis')
    df_comments['Comment'] = df_comments['Comment'].apply(cleanTxt)
    df_comments['Subjectivity'] = df_comments['Comment'].apply(getSubjectivity)
    df_comments['Polarity'] = df_comments['Comment'].apply(getPolarity)
    df_comments['Analysis'] = df_comments['Polarity'].apply(getAnalysis)
    st.text(df_comments)
    allWords = ' '.join([cmts for cmts in data['Comment']])
    wordCloud = WordCloud(width=500, height=300, random_state=21, max_font_size=119).generate(allWords)
    plt.imshow(wordCloud, interpolation='bilinear')
    plt.axis("off")
    # plt.show()
    st.set_option('deprecation.showPyplotGlobalUse', False)
    st.pyplot()
    st.text('Total Comments : ' + str(len(data)))
    st.text('Positive : ' + str(positive))
    st.text('Negative : ' + str(negative))
    st.text('Neutral : ' + str(neutral))

    # plt.title('Sentiment Analysis')
    # plt.xlabel('Sentiment')
    # plt.ylabel('Counts')
    # data['Analysis'].value_counts().plot(kind='bar')
    # st.pyplot()

