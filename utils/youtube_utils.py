from googleapiclient.discovery import build
import pandas as pd
import seaborn as sns

# Get the Api key
apikey = 'AIzaSyAvDyM4PVSaEWGheInZyvD7JWuWttBHqfg'
youtube = build('youtube', 'v3', developerKey=apikey)


def all_channel_stat(youtube, channel_ids):
    request = youtube.channels().list(
        part='snippet,contentDetails,statistics',
        id=channel_ids)

    response = request.execute()
    return response


### Function to get channel details
def all_channel_details(youtube, channel_ids):
    all_data = []
    request = youtube.channels().list(
        part='snippet,contentDetails,statistics',
        id=channel_ids)
    response = request.execute()

    for i in range(len(response['items'])):
        data = dict(Channel_name=response['items'][i]['snippet']['title'],
                    Subscribers=response['items'][i]['statistics']['subscriberCount'],
                    Views=response['items'][i]['statistics']['viewCount'],
                    Total_videos=response['items'][i]['statistics']['videoCount'])
        all_data.append(data)

    return all_data


def all_channel_details2(youtube, channel_ids):
    all_data1 = []
    request = youtube.channels().list(
        part='snippet,contentDetails,statistics',
        id=channel_ids)
    response = request.execute()

    for i in range(len(response['items'])):
        data = dict(Channel_name=response['items'][i]['snippet']['title'],
                    Subscribers=response['items'][i]['statistics']['subscriberCount'],
                    Views=response['items'][i]['statistics']['viewCount'],
                    Total_videos=response['items'][i]['statistics']['videoCount'],
                    playlist_id=response['items'][i]['contentDetails']['relatedPlaylists']['uploads'])
        all_data1.append(data)

    return all_data1


def get_video_id(youtube, playlist_id):
    request = youtube.playlistItems().list(
        part='contentDetails',
        playlistId=playlist_id,
        maxResults=50)
    response = request.execute()

    video_ids = []
    for i in range(len(response['items'])):
        video_ids.append(response['items'][i]['contentDetails']['videoId'])
    nextpagetoken = response.get('nextPageToken')
    more_pages = True
    while more_pages:

        if nextpagetoken is None:
            more_pages = False
        else:
            request = youtube.playlistItems().list(
                part='contentDetails',
                playlistId=playlist_id,
                maxResults=50,
                pageToken=nextpagetoken)
            response = request.execute()
            for i in range(len(response['items'])):
                video_ids.append(response['items'][i]['contentDetails']['videoId'])
            nextpagetoken = response.get('nextPageToken')

    return video_ids


def video_details(youtube, video_ids):
    request = youtube.videos().list(
        part='snippet,statistics',
        id=','.join(video_ids[:50]))
    response = request.execute()

    return response


def get_video_name(youtube, video_ids):
    all_video_info = []

    for i in range(0, len(video_ids), 50):
        request = youtube.videos().list(
            part='snippet,statistics',
            id=','.join(video_ids[i:i + 50]))

        response = request.execute()

        for video in response['items']:
            video_statistics = dict(Title=video['snippet']['title'],
                                    Published_date=video['snippet']['publishedAt'],
                                    Views=video['statistics']['viewCount'],
                                    Likes=video['statistics']['likeCount'],
                                    Favourites=video['statistics']['favoriteCount'],
                                    Comments=video['statistics']['commentCount']
                                    )
            all_video_info.append(video_statistics)

    return all_video_info
