import pafy
from googleapiclient.discovery import build
import pandas as pd
import seaborn as sns
from youtube_transcript_api import YouTubeTranscriptApi
import random

# Get the Api key
apikey = 'AIzaSyAvDyM4PVSaEWGheInZyvD7JWuWttBHqfg'
youtube = build('youtube', 'v3', developerKey=apikey)


def get_thumbnail(url):
    thumbnail = (pafy.new(url)).bigthumbhd
    return thumbnail


def video_details(youtube, video_id):
    request = youtube.videos().list(
        part='snippet,statistics',
        id=video_id)
    response = request.execute()

    return response


def video_statistic(response):
    all_video_info = []
    video = response['items'][0]
    video_statistics = dict(Title=video['snippet']['title'],
                            Description=video['snippet']['description'],
                            Published_date=video['snippet']['publishedAt'],
                            Views=video['statistics']['viewCount'],
                            Likes=video['statistics']['likeCount'],
                            Favourites=video['statistics']['favoriteCount'],
                            CountComment=video['statistics']['commentCount'],
                            ChannelTitle=video['snippet']['channelTitle'],
                            Tags=video['snippet']['tags']
                            )

    return video_statistics


def scrape_comments_with_replies(video_id):
    box = [['Name', 'Comment', 'Time', 'Likes', 'Reply Count']]
    data = youtube.commentThreads().list(part='snippet', videoId=video_id, maxResults='100',
                                         textFormat="plainText").execute()

    for i in data["items"]:
        name = i["snippet"]['topLevelComment']["snippet"]["authorDisplayName"]
        comment = i["snippet"]['topLevelComment']["snippet"]["textDisplay"]
        published_at = i["snippet"]['topLevelComment']["snippet"]['publishedAt']
        likes = i["snippet"]['topLevelComment']["snippet"]['likeCount']
        replies = i["snippet"]['totalReplyCount']

        box.append([name, comment, published_at, likes, replies])

        totalReplyCount = i["snippet"]['totalReplyCount']

        if totalReplyCount > 0:

            parent = i["snippet"]['topLevelComment']["id"]

            data2 = youtube.comments().list(part='snippet', maxResults='100', parentId=parent,
                                            textFormat="plainText").execute()

            for i in data2["items"]:
                name = i["snippet"]["authorDisplayName"]
                comment = i["snippet"]["textDisplay"]
                published_at = i["snippet"]['publishedAt']
                likes = i["snippet"]['likeCount']
                replies = ""

                box.append([name, comment, published_at, likes, replies])

    while ("nextPageToken" in data):

        data = youtube.commentThreads().list(part='snippet', videoId=video_id, pageToken=data["nextPageToken"],
                                             maxResults='100', textFormat="plainText").execute()

        for i in data["items"]:
            name = i["snippet"]['topLevelComment']["snippet"]["authorDisplayName"]
            comment = i["snippet"]['topLevelComment']["snippet"]["textDisplay"]
            published_at = i["snippet"]['topLevelComment']["snippet"]['publishedAt']
            likes = i["snippet"]['topLevelComment']["snippet"]['likeCount']
            replies = i["snippet"]['totalReplyCount']

            box.append([name, comment, published_at, likes, replies])

            totalReplyCount = i["snippet"]['totalReplyCount']

            if totalReplyCount > 0:

                parent = i["snippet"]['topLevelComment']["id"]

                data2 = youtube.comments().list(part='snippet', maxResults='100', parentId=parent,
                                                textFormat="plainText").execute()

                for i in data2["items"]:
                    name = i["snippet"]["authorDisplayName"]
                    comment = i["snippet"]["textDisplay"]
                    published_at = i["snippet"]['publishedAt']
                    likes = i["snippet"]['likeCount']
                    replies = ''

                    box.append([name, comment, published_at, likes, replies])
    box.pop(0)
    df = pd.DataFrame({'Name': [i[0] for i in box], 'Comment': [i[1] for i in box], 'Time': [i[2] for i in box],
                       'Likes': [i[3] for i in box], 'Reply Count': [i[4] for i in box]})
    # sql_vids = pd.DataFrame([])
    # sql_vids = sql_vids.append(df, ignore_index=True)
    return df


def get_transcript(video_id):
    transcript_list = YouTubeTranscriptApi.list_transcripts(video_id)
    for transcript in transcript_list:
        full_script = transcript.translate('en').fetch()

    script = full_script[0].get('text')
    return script


def get_type_of_video(video_id):
    mappings = { 1 : 'Educational',
                 2 : 'Company',
                 3 : 'Product Videos',
                 4 : 'Testimonial videos',
                 5 : 'Promotional Video'}
    label = random.randrange(1,5)
    return mappings[label]