import streamlit as st
import matplotlib.pyplot as plt
from utils.youtube_utils import *
import seaborn as sns

channel_ids = st.text_input('Youtube Channel ID')
if channel_ids:
    df_channel_details = all_channel_details(youtube, channel_ids)
    channel_data = pd.DataFrame(df_channel_details)
    st.text('Channel Information')
    st.dataframe(channel_data)
    channel_data['Subscribers'] = pd.to_numeric(channel_data['Subscribers'])
    channel_data['Views'] = pd.to_numeric(channel_data['Views'])
    channel_data['Total_videos'] = pd.to_numeric(channel_data['Total_videos'])
    sns.set(rc={'figure.figsize': (10, 8)})
    ax = sns.barplot(x='Channel_name', y='Subscribers', data=channel_data)
    st.pyplot(ax.figure)
    df2 = all_channel_details2(youtube, channel_ids)
    playlist_id = df2[0]['playlist_id']
    video_ids = get_video_id(youtube, playlist_id)
    video_details = get_video_name(youtube, video_ids)
    video_data = pd.DataFrame(video_details)
    st.text('Channel Videos')
    st.dataframe(video_data)
    video_data['Published_date'] = pd.to_datetime(video_data['Published_date']).dt.date
    video_data['Views'] = pd.to_numeric(video_data['Views'])
    video_data['Likes'] = pd.to_numeric(video_data['Likes'])
    video_data['Favourites'] = pd.to_numeric(video_data['Favourites'])
    video_data['Comments'] = pd.to_numeric(video_data['Comments'])
    top_ten_videos = video_data.sort_values(by='Views', ascending=False).head(10)
    st.text('Top 10 videos')
    ax1 = sns.barplot(x='Views', y='Title', data=top_ten_videos)
    st.pyplot(ax1.figure)
    video_data['Month'] = pd.to_datetime(video_data['Published_date']).dt.strftime('%b')
    st.text('Video by month')
    st.dataframe(video_data)
    uploaded_video_per_month = video_data.groupby('Month', as_index=False).size()
    st.dataframe(uploaded_video_per_month)
    sort_order = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    uploaded_video_per_month.index = pd.CategoricalIndex(uploaded_video_per_month['Month'], categories=sort_order,
                                                         ordered=True)
    uploaded_video_per_month = uploaded_video_per_month.sort_index()
